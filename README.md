# Artix-efiboot

This is clone of the [arch-efiboot](https://github.com/RobertCsordas/arch-efiboot), but with different path of `$EFISTUB` to support non-systemd Artix. Before you build this package, you need to:
- download [systemd](https://archlinux.org/packages/core/x86_64/systemd/) tarball and extract it (I use version `251.7-4`, which you can get from [archlinux archive](https://archive.archlinux.org/packages/s/systemd/), since newest stub didn't work for me),
- copy `usr/lib/systemd/boot/efi/linuxx64.efi.stub` to `/boot/linuxx64.efi.stub` (notice it's an efi stub, not an elf).
